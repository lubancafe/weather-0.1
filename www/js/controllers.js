angular.module('starter.controllers', ['ionic'])
.constant('FORECASTIO_KEY', '785864172151072bf3ac70e2d5b730b3')

.controller('HomeCtrl', function($scope,$state,$ionicLoading,Weather,DataStore) {

  $scope.$on('$ionicView.enter', function(){
    //this is to fix ng-repeat slider width:0px;
    console.log('inside home');
    
    $ionicLoading.hide();

    $scope.city  = DataStore.city;
    var latitude  =  DataStore.latitude;
    var longitude = DataStore.longitude;

    //call getCurrentWeather method in factory ‘Weather’
    Weather.getCurrentWeather(latitude,longitude).then(function(resp) {
      $scope.current = resp.data;
      console.log('GOT CURRENT', $scope.current);
      //debugger;
    }, function(error) {
      alert('Unable to get current conditions');
      console.error(error);
    });
    
  });

  $scope.doRefresh = function() {
    console.log('inside home doRefresh');
    $scope.city  = DataStore.city;
    var latitude  =  DataStore.latitude;
    var longitude = DataStore.longitude;

    $ionicLoading.show({
      template: 'Loading ...'
    });

    //Always bring me the latest Weather
    Weather.getCurrentWeather(latitude, longitude)
    .then(function(resp){

      $scope.current = resp.data;
      console.log('GOT CURRENT', $scope.current);

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }, function(error) {
      alert('Unable to get current conditions');
      console.error(error);
    });
  };

})

.controller('LocationsCtrl', function($scope,$state, Cities,DataStore) {
  $scope.cities = Cities.all();

  $scope.changeCity = function(cityId) {
    //get lat and longitude for seleted location
    var lat  = $scope.cities[cityId].lat; //latitude
    var lgn  = $scope.cities[cityId].lgn; //longitude
    var city = $scope.cities[cityId].name; //city name

    DataStore.setCity(city);
    DataStore.setLatitude(lat);
    DataStore.setLongitude(lgn);

    $state.go('tab.home');
  }
})
.controller('SettingsCtrl', function($scope) {
    //manages app settings
});